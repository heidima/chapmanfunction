import chapman_mapping as ch

class ChapLiterature:
    @staticmethod 
    def fitzmaurice(x, zeta):
        """Chapman function as approximated by Fitzmaurice
        Fitzmaurice, "Simplfication of the Chapman
		    Function for Atmospheric Attenuation," Appl. Opt. **3**, 640 (1964).
        :param
            x: scale parameter X=(R+z)/H
            zeta: solar zenith angle in degrees
        :return
            value of Chapman mapping function (approximation)
        """
        chap = ch.chaplit.fitzmaurice(x=x, theta=zeta)
        return chap

    @staticmethod
    def huestis(x, zeta):
        """Chapman function as approximated by 1st term asymptotic expansion of Huestis
        Huestis, "Accurate evaluation of the Chapman function for atmospheric attenuation",
            J. Quant. Spectr. Radiat. Transfer **69**, 709-721 (2001).
        :param
            x: scale parameter X=(R+z)/H
            zeta: solar zenith angle in degrees
        :return
            value of Chapman mapping function (approximation)
        """
        chap = ch.chaplit.huestis(x=x, chi0=zeta)
        return chap

    @staticmethod
    def swider(x, zeta):
        """Chapman function as approximated by Swider
        Swider,  "The Determination of the Optical
        	Depth at Large Solar Zenith Angles," Planet. Space
     		Sci. **12**, 761-782 (1964).
        :param
            x: scale parameter X=(R+z)/H
            zeta: solar zenith angle in degrees
        :return
            value of Chapman mapping function (approximation)
        """
        chap = ch.chaplit.swider(x=x, chi0=zeta)
        return chap

    @staticmethod
    def kocifaj(x, zeta):
        """Chapman function as approximated by Kocifaj
        Kocifaj, "Optical air mass and refraction in a Rayleigh
		        atmosphere", Contrib. Astron. Obs. Skalnate Pleso **26**, 23-30 (1996).
        :param
            x: scale parameter X=(R+z)/H
            zeta: solar zenith angle in degrees
        :return
            value of Chapman mapping function (approximation)
        """
        chap = ch.chaplit.kocifaj(x=x, chi0=zeta)
        return chap

    @staticmethod
    def green(x, zeta):
        """Chapman function as approximated by Green and Barnum
        Green, Lindenmeyer, Griggs, "Molecular absorption in molecular atmospheres",
                J. Geophys. Res.  **69**, 493-504 (1964).
        :param
            x: scale parameter X=(R+z)/H
            zeta: solar zenith angle in degrees
        :return
            value of Chapman mapping function (approximation)
        """
        chap = ch.chaplit.green(x=x, chi0=zeta)
        return chap
    
class ProposedApprox:
    @staticmethod
    def chap_uniform_as(x, zeta):
        """Chapman function as approximated by current work
        :param
            x: scale parameter X=(R+z)/H
            zeta: solar zenith angle in degrees
        :return
            value of Chapman mapping function (approximation)
        """
        chap = ch.approximation.chapman_uniform_asymptotic(x=x, chi0=zeta)
        # alternatively:
        # chap = ch.approximation.chapman_k1_expression(x=x, chi0=zeta)
        # or less accurate
        # chap = ch.approximation.chapman_k2k0_expression(x=x, chi0=zeta)
        return chap


if __name__ == '__main__':

    #print(ch.__doc__) # info on the package structure
    # scale parameter
    x = 50.
    # solar zenith angle (deg)
    zeta = 40.
    print(f"Sample values for arguments: X={x}, zeta={zeta}")
    print(f"Ch(X, zeta)={ch.chapman.atm8_chap_num(x,zeta)}  <-- Numerical integration (exact)")
    print(f"Ch(X, zeta)={ChapLiterature.green(x, zeta)} <-- Green & Barnum  approximation")
    print(f"Ch(X, zeta)={ChapLiterature.fitzmaurice(x, zeta)} <-- Fitzmaurice approximation")
    print(f"Ch(X, zeta)={ChapLiterature.huestis(x, zeta)} <-- Huestis approximation")
    print(f"Ch(X, zeta)={ChapLiterature.swider(x, zeta)} <-- Swider approximation")
    print(f"Ch(X, zeta)={ChapLiterature.kocifaj(x, zeta)} <-- Kocifaj approximation")
    print(f"Ch(X, zeta)={ProposedApprox.chap_uniform_as(x, zeta)} <-- proposed approximation")