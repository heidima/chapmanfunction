!C*********************************************************************C
!C*                                                                   *C
!C*  util.f90                                                         *C
!C*                                                                   *C
!C*  Written by:  David L. Huestis, Molecular Physics Laboratory      *C
!C*                                                                   *C
!C*  Copyright (c) 2000  SRI International                            *C
!C*  All Rights Reserved                                              *C
!C*                                                                   *C
!C*  Expanded with special functions from Press et al.                *C
!C*                                                                   *C
!C*  Approximations for the Incomplete Bessel functions               *C
!C*  are written by D. Vasylyev, German Aerospace Center (DLR)        *C
!C*                                                                   *C
!C*  Copyright (c) 2021  DLR                                          *C
!C*  All Rights Reserved                                              *C
!C*                                                                   *C
!C*  This software is provided on an as is basis; without any         *C
!C*  warranty; without the implied warranty of merchantability or     *C
!C*  fitness for a particular purpose.                                *C
!C*                                                                   *C
!C*********************************************************************C
    
    module utilities
    contains
    
!C  ####################################################################
!C
!C	The following "pure math" routines return various combinations
!C	of Bessel functions, powers, and exponentials.
!C
!C  ####################################################################
!
!C  ====================================================================
!C
!C	This Bessel function math routine returns
!C
!C	  xI1 = exp(-|z|) * I1(z)
!C
!C	Following Press et al [PFT86, page 178, BESSI1.FOR] and 
!C	Abrahamson and Stegun [AS64, page 378, 9.8.3, 9.8.4].
!C
!C  ====================================================================

	real*8 function atm8_chap_xI1( z )
	implicit real*8(a-h,o-z)
    real*8, intent(in):: z
        dimension aI1(0:6), bI1(0:8)

        data aI1/ 5.00000000D-01, 6.2499978D-02, 2.6041897D-03, &
            5.4244512D-05, 6.7986797D-07, 5.4830314D-09, &
            4.1909957D-11/
        data bI1/ 3.98942280D-01,-1.4955090D-01,-5.0908781D-02, &
            8.6379434D-02,-2.0399403D+00, 1.6929962D+01, &
           -8.0516146D+01, 1.8642422D+02,-1.6427082D+02/

	if( z .lt. 0 ) then
	  az = -z
	else if( z .eq. 0 ) then
	  atm8_chap_xI1 = 0
	  return
	else
	  az = z
	end if
	if( az .lt. 3.75d0 ) then
	  z2 = z*z
	  sum = aI1(6)
	  do i=5,0,-1
	    sum = sum*z2 + aI1(i)
	  end do
	  atm8_chap_xI1 = z*exp(-az) * sum
	else
	  sum = bI1(8)
	  do i=7,0,-1
	    sum = sum/az + bI1(i)
	  end do
	  atm8_chap_xI1 = sum*sqrt(az)/z
	end if
	return
	end

!C  ====================================================================
!C
!C	This Bessel function math routine returns
!C
!C	  xK1 = z * exp(+z) * K1(z)
!C
!C	Following Press et al [PFT86, page 179, BESSK1.FOR] and 
!C	Abrahamson and Stegun [AS64, page 379, 9.8.7, 9.8.8].
!C
!C  ====================================================================

	real*8 function atm8_chap_xK1( z )
	implicit real*8(a-h,o-z)
        dimension aK1(0:6), bK1(0:6)

        data aK1/ 1.00000000D+00, 3.8607860D-02,-4.2049112D-02, &
           -2.8370152D-03,-7.4976641D-05,-1.0781641D-06, &
           -1.1440430D-08/
        data bK1/ 1.25331414D+00, 4.6997238D-01,-1.4622480D-01, &
            1.2034144D-01,-1.2485648D-01, 1.0419648D-01, &
           -4.3676800D-02/

	if( z .le. 0 ) then
	  atm8_chap_xK1 = 1
	else if( z .lt. 2 ) then
	  xz = exp(z)
	  z2 = z*z
	  sum = aK1(6)
	  do i=5,0,-1
	    sum = sum*z2 + aK1(i)
	  end do
	  atm8_chap_xK1 = xz * ( sum &
      		+ z*log(z/2)*atm8_chap_xI1(z)*xz ) 
	else
	  sum = bk1(6)
	  do i=5,0,-1
	    sum = sum/z + bK1(i)
	  end do
	  atm8_chap_xK1 = sum*sqrt(z)
	end if

	return
	end

!C  ####################################################################
!C
!C	The following "pure math" routines return various combinations
!C	of the Error function, powers, and exponentials.
!C
!C  ####################################################################
!
!C  ====================================================================
!C
!C	This Error function math routine returns
!C
!C	  xerfc(x) = exp(x**2)*erfc(x)
!C
!C	following Press et al. [PFT86, p164, ERFCC.FOR]
!C
!C  ====================================================================

	real*8 function atm8_chap_xerfc(x)
	implicit real*8(a-h,o-z)
        T=1.0D0/(1.0D0+0.5D0*x)
	atm8_chap_xerfc = &
      	  T*EXP( -1.26551223D0 +T*(1.00002368D0 +T*( .37409196D0 &
             +T*(  .09678418D0 +T*(-.18628806D0 +T*( .27886807D0 &
      	     +T*(-1.13520398D0 +T*(1.48851587D0 +T*(-.82215223D0 &
      	     +T*   .17087277D0) ))))))))
        RETURN
        END

!C  ####################################################################
!C
!C	The following "pure math" routines return various combinations
!C	of Exponential integrals, powers, and exponentials.
!C
!C  ####################################################################
!
!C  ====================================================================
!C
!C	This Exponential math routine evaluates
!C
!C	  zxE1(x) = x*exp(x) int{y=1,infinity} [ exp(-x*y)/y dy ]
!C
!C	following Abramowitz and Stegun [AS64, p229;231, equations
!C	5.1.11 and 5.1.56]
!C
!C  ====================================================================

	real*8 function atm8_chap_zxE1(x)
	implicit real*8(a-h,o-z)
	parameter (gamma = 0.5772156649015328606d0)
	dimension aE1(0:4), bE1(0:4), cEin(1:10)

	data aE1/1.0d0, 8.5733287401d0, 18.0590169730d0, &
      	    8.6347608925d0, 0.2677737343d0 /
	data bE1/1.0d0, 9.5733223454d0, 25.6329561486d0, &
      	    21.0996530827d0, 3.9584969228d0/
        data cEin/ 1.00000000D+00,-2.50000000D-01, 5.55555556D-02, &
          -1.0416666667D-02, 1.6666666667D-03,-2.3148148148D-04, &
           2.8344671202D-05,-3.1001984127D-06, 3.0619243582D-07, &
          -2.7557319224D-08/

	if( x .le. 0 ) then
	  atm8_chap_zxE1 = 0
	else if( x .le. 1 ) then
	  sum = cEin(10)
	  do i=9,1,-1
	    sum = sum*x + cEin(i)
	  end do
	  atm8_chap_zxE1 = x*exp(x)*( x * sum - log(x) - gamma )
	else
	  top = aE1(4)
	  bot = bE1(4)
	  do i=3,0,-1
	    top = top/x + aE1(i)
	    bot = bot/x + bE1(i)
	  end do
	  atm8_chap_zxE1 = top/bot
	end if
	return
	end

!C  ####################################################################
!C
!C	The following "pure math" routines return various combinations
!C	of incomplete gamma functions, powers, and exponentials.
!C
!C  ####################################################################
!
!C  ====================================================================
!C
!C	This gamma function math routine calculates
!C
!C	Dn(n) = int{t=z,infinity}
!C		[ exp( -(t**2-z**2) ) * (t**2-z**2)**n dt ]
!C
!C  ====================================================================

	subroutine atm8_chap_gd3( z, Dn )
	implicit real*8(a-h,o-z)
	parameter (rpi=1.7724538509055160273d0)
	dimension Dn(0:3), xg(0:3)

	if( z .le. 0 ) then
	  Dn(0) = rpi/2
	  do i=1,3
	    Dn(i) = (i-0.5d0)*Dn(i-1)
	  end do
	  return
	end if

	z2 = z*z
	if( z .ge. 7 ) r = 1/z2

	if( z .lt. 14 ) then
	  z4 = z2*z2
	  xg(0) = rpi * atm8_chap_xerfc(z)
	  xg(1) = 0.5d0*xg(0) + z
	  xg(2) = 1.5d0*xg(1) + z*z2
	  Dn(0) = 0.5d0*xg(0)
	  Dn(1) = 0.5d0*(xg(1)-z2*xg(0))
	  Dn(2) = 0.5d0*(xg(2)-2*z2*xg(1)+z4*xg(0))
	else
	  Dn(0) = ( 1 + r*(-0.5d0 +r*(0.75d0 +r*(-1.875d0 &
     		+r*6.5625d0) ) ) )/(2*z)
	  Dn(1) = ( 1 + r*(-1.0d0 +r*(2.25d0 +r*(-7.5d0 &
     		+r*32.8125d0) ) ) )/(2*z)
	  Dn(2) = ( 2 + r*(-3.0d0 +r*(9.00d0 +r*(-37.5d0 &
     		+r*196.875d0) ) ) )/(2*z)
	end if

	if( z .lt. 7 ) then
	  z6 = z4*z2
	  xg(3) = 2.5d0*xg(2) + z*z4
	  Dn(3) = 0.5d0*(xg(3)-3*z2*xg(2)+3*z4*xg(1)-z6*xg(0))
	else
	  Dn(3) = ( 6 + r*(-12.0d0 +r*(45.0d0 +r*(-225.0d0 &
      		+r*1378.125d0) ) ) )/(2*z)
	end if

	return
	end

!C  ====================================================================
!C
!C	This Gamma function math routine calculates
!C
!C	  gf06(n) = g(n,x) * int{y=x,z} [log(y) * exp(-y) * y**(2*n) dy]
!C
!C	and
!C
!C	  gg06(n) = g(n,x) * int{y=x,z} [ exp(-y) * y**(2*n) dy ]
!C	          = g(n,x) * ( Gamma(2*n+1,x) - Gamma(2*n+1,z) )
!C
!C	for n=0,6, with g(n,x) = exp(x) * max(1,x)**(-2*n)
!C
!C  ====================================================================

	subroutine atm8_chap_gfg06( x, z, gf06, gg06 )
	implicit real*8 (a-h,o-z)
	parameter (gamma = 0.5772156649015328606d0)
	dimension gf06(0:6), gg06(0:6)
	dimension gh13x(13), gh13z(13), rgn(13), delta(13)
	call atm8_chap_gh13( x, x, gh13x )
	call atm8_chap_gh13( x, z, gh13z )
	if( x .le. 1 ) then
	  rho = 1
	else
	  rho = 1/x
	end if

	delta(1) = 0
	delta(2) = ( gh13x(1) - gh13z(1) ) * rho
	rgn(1) = 1
	rgn(2) = rho
	do n=2,12
	  delta(n+1) = rho*( n*delta(n) + gh13x(n) - gh13z(n) )
	  rgn(n+1) = (n*rho)*rgn(n)
	end do

	if( x .gt. 0 ) then
	  xE1_x = atm8_chap_zxE1(x)/x
	  xlog = log(x)
	end if
	if( z .gt. 0 ) then
	  xE1_z = exp(x-z)*atm8_chap_zxE1(z)/z
	  zlog = log(z)
	end if

	do k=0,6
	  n = 2*k+1
	  if( x .le. 0 ) then
	    gf06(k) = -gamma*rgn(n) + delta(n)
	  else
	    gf06(k) = xlog*gh13x(n) + rgn(n)*xE1_x + delta(n)
	  end if
	  if( z .le. 0 ) then
	    gf06(k) = gf06(k) + gamma*rgn(n)
	  else
	    gf06(k) = gf06(k) - (zlog*gh13z(n) + rgn(n)*xE1_z)
	  end if
	  gg06(k) = gh13x(n) - gh13z(n)
	end do

	return
    end

!C  ====================================================================
!C
!C	This Gamma function math routine calculates
!C
!C	  gg06(n) = g(n,x) * int{y=x,z} [ exp(-y) * y**(2*n) dy ]
!C	          = g(n,x) * ( Gamma(2*n+1,x) - Gamma(2*n+1,z) )
!C
!C	for n=0,6, with g(n,x) = exp(x) * max(1,x)**(-2*n)
!C
!C  ====================================================================

    
    subroutine atm8_chap_gg06( x, z, gg06 )
	implicit real*8 (a-h,o-z)
	dimension gg06(0:6), gh13x(13), gh13z(13)
	call atm8_chap_gh13( x, x, gh13x )
	call atm8_chap_gh13( x, z, gh13z )
	do n=0,6
	  gg06(n) = gh13x(2*n+1) - gh13z(2*n+1)
	end do
	return
	end

!C  ====================================================================
!C
!C	This Gamma function math routine calculates
!C
!C	  gh13(n) = f(n,x) * int{y=z,infinity} [exp(-y) * y**(n-1) dy]
!C	          = f(n,x) * Gamma(n,z)
!C
!C	for n=1,13, with f(n,x) = exp(x) * max(1,x)**(-n+1)
!C
!C  ====================================================================

	subroutine atm8_chap_gh13( x, z, gh13 )
	implicit real*8 (a-h,o-z)
	dimension gh13(13), Tab(12)

	if( z .le. 0 ) then
	  gh13(1) = 1
	  do n=1,12
	    gh13(n+1) = n*gh13(n)
	  end do
	  return
	end if

	if( x .le. 1 ) then
	  rho = 1
	else
	  rho = 1/x
	end if
	rhoz = rho * z
	exz = exp(x-z)
	Tab(12) = exp( (x-z) + 12*log(rhoz) )
	do n=11,1,-1
	  Tab(n) = Tab(n+1)/rhoz
	end do
	gh13(1) = exz
	do n=1,12
	  gh13(n+1) = rho*n*gh13(n) + Tab(n)
	end do
	return
	end

!C  ====================================================================
!C
!C	This Gamma function math subroutine calculates
!C
!C	  Qn(x) = x**n * exp(x) * Gamma(-n+0.5,x), n=0,8
!C	    = x**n * exp(x) * int{y=x,infinity} [exp(-y)*y**(-n-0.5)dy]
!C
!C	For x .lt. 2 we first calculate
!C
!C	  Q0(x) = sqrt(pi)*exp(x)*erfc(sqrt(x)) = exp(x)*Gamma(0.5,x)
!C
!C	and use upward recursion.  Else, we first calculate
!C
!C	  Q8(x) = x**8 * exp(x) * Gamma(-7.5,x)
!C
!C	following Press et al. [PFT86, pp162-63, GCF.FOR] and then
!C	recur downward.  Also see Abramowitz and Stegun [AS64, 6.5].
!C
!C  ====================================================================

	subroutine atm8_chap_gq85( x, qn )
	implicit real*8(a-h,o-z)
	parameter (rpi=1.7724538509055160273d0)
	parameter (itmax=100,eps=3.0d-9)
	dimension qn(0:8)

	if( x .le. 0 ) then
	  qn(0) = rpi
	  do i=1,8
	    qn(i) = 0
	  end do
	  return
	end if

	rx = sqrt(x)

	if( x .lt. 2 ) then
	  qn(0) = rpi * atm8_chap_xerfc( rx )
	  do n=1,8
	    qn(n) = ( -rx*qn(n-1) + 1 ) * rx / ( n - 0.5d0 )
	  end do
	else
          GOLD=0.0d0
	  A0=1.0d0
	  A1=x
	  B0=0.0d0
	  B1=1.0d0
	  FAC=1.0d0
	  DO 11 N=1,ITMAX
	    AN= (N)
	    ANA=AN + 7.5d0
	    A0=(A1+A0*ANA)*FAC
	    B0=(B1+B0*ANA)*FAC
	    ANF=AN*FAC
	    A1=x*A0+ANF*A1
	    B1=x*B0+ANF*B1
	    FAC=1./A1
            G=B1*FAC
	    test = G*eps
	    del = G - Gold
	    if( test .lt. 0 ) test = - test
	    if( (del .ge. -test) .and. (del .le. test) ) go to 12
	    GOLD=G
11        CONTINUE
12	  qn(8) = G * rx
	  do n=8,1,-1
	    qn(n-1) = ( (-n+0.5d0)*qn(n)/rx + 1 ) / rx
	  end do
	end if

	return
    end
    
    !  ====================================================================
    !
    ! The following code for the calculation of Bessel functions I_0, I_1, K_0, K_1 
    ! is adapted from 
    ! W. H. Press, S. A. Teukolsky, W. T. Vetterling, and B. P. Flannery, "Numerical Recipes in Fortran", 
    ! second edition, Cambridge University Press, Cambridge, 1992.
    !
    !  ====================================================================
    
    ! Bessel function I0(x) from Press et al.
    real*8 function bessi0(x)
		real*8, intent(in) :: x
		real*8 :: p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7,q8,q9,y
		save p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7,q8,q9
		data p1,p2,p3,p4,p5,p6,p7/ 1.0d0, 3.5156229d0, 3.0899424d0, 1.2067492d0, &
			0.2659732d0, 0.360768d-1, 0.45813d-2/
		data q1,q2,q3,q4,q5,q6,q7,q8,q9 /0.39894228d0, 0.1328592d-1, &
			0.225319d-2, -0.157565d-2,0.916281d-2, -0.2057706d-1, &
			0.2635537d-1, -0.1647633d-1, 0.392377d-2/
		if (abs(x).le.3.75) then
			y=(x/3.75)**2
			bessi0 =p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7)))))
		else
			ax = abs(x)
			y = 3.75/ax
			bessi0 =(exp(ax)/sqrt(ax))*(q1+y*(q2+y*(q3+y*(q4 &
				+y*(q5+y*(q6+y*(q7+y*(q8+y*q9))))))))
		endif
    return 
    end
    
    ! Bessel function K0(x) from Press et al.
    real*8 function bessk0(x)
		real*8, intent(in) :: x
		real*8 :: p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7, y
		save p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7
		data p1,p2,p3,p4,p5,p6,p7 /-0.57721566d0, 0.42278420d0, 0.23069756d0,&
			0.3488590d-1, 0.262698d-2, 0.10750d-3, 0.74d-5/
		data q1, q2, q3, q4, q5, q6, q7 /1.25331414d0, -0.7832358d-1, 0.2189568d-1,&
			-0.1062446d-1, 0.587872d-2, -0.251540d-2, 0.53208d-3/
		if (x.le.2.d0) then
			y = x*x/4.0
			bessk0 = (-log(x/2.d0)*bessi0(x))+(p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7))))))
		else
			y = (2.d0/x)
			bessk0 = (exp(-x)/sqrt(x))*(q1+y*(q2+y*(q3+y*(q4+y*(q5+y*(q6+y*q7))))))
		endif
    return 
    end
    
    
    
    
    ! Bessel function I1(x) from Press et al.
    real*8 function bessi1(x)
		real*8, intent(in):: x
		real*8 :: ax
		real*8 :: p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7,q8,q9,y
		save p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7,q8,q9
		data p1,p2,p3,p4,p5,p6,p7 /0.5d0, 0.87890594d0, 0.51498869d0, &
			0.15084934d0, 0.2658733d-1,0.301532d-2, 0.32411d-3/
		data q1,q2,q3,q4,q5,q6,q7,q8,q9 /0.39894228d0, -0.3988024d-1, &
			-0.362018d-2, 0.163801d-2, -0.1031555d-1, 0.2282967d-1, &
			-0.2895312d-1, 0.1787654d-1, -0.420059d-2/
		if (abs(x).lt. 3.75) then
			y=(x/3.75)**2
			bessi1 =x*(p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7))))))
		else
			ax = abs(x)
			y = 3.75/ax
			bessi1 =(exp(ax)/sqrt(ax))*(q1+y*(q2+y*(q3+y*(q4 &
				+y*(q5+y*(q6+y*(q7+y*(q8+y*q9))))))))
			if (x.lt.0.d0) bessi1 = -bessi1
		endif        
    return
    end
    
    ! Bessel function K1(x) from Press et al.
    real*8 function bessk1(x)
		real*8, intent(in) :: x
		real*8 :: p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7, y
		save p1,p2,p3,p4,p5,p6,p7,q1,q2,q3,q4,q5,q6,q7
		data p1,p2,p3,p4,p5,p6,p7 /1.0d0, 0.15443144d0, -0.67278579d0,&
			-0.18156897d0, -0.1919402d-1, -0.110404d-2, -0.4686d-4/
		data q1, q2, q3, q4, q5, q6, q7 /1.25331414d0, 0.23498619d0, -0.3655620d-1,&
			0.1504268d-1, -0.780353d-2, 0.325614d-2, -0.68245d-3/
		if (x.le.2.d0) then
			y = x*x/4.0
			bessk1 = (log(x/2.d0)*bessi1(x))+(1.0/x)*(p1+y*(p2+y*(p3+y*(p4+y*(p5+y*(p6+y*p7))))))
		else
			y = (2.d0/x)
			bessk1 = (exp(-x)/sqrt(x))*(q1+y*(q2+y*(q3+y*(q4+y*(q5+y*(q6+y*q7))))))

		endif
    return 
    end
    
    
    !  ====================================================================
    !
    ! The following code evaluates the analytic approximation for the incomplete
    ! Weber integral $\tilde Q_0(x,y)$
    ! The approximation is discussed in detail in
    ! D. Vasylyev, A. Semenov, and W. Vogel, "Quantum channels with beam wandering:
    !	an analysis of the Marcum Q-function", Phys. Scripta _T153_, 014062 (2013).
    !
    !  ====================================================================
    
    
    ! evaluates auxilary function
    real*8 function iv0(x)
        real*8::x
        iv0 = 1.0d0/(1.0d0-exp(-x)*bessi0(x))
        return
    end
    
    ! evaluates the shape parameter lambda
    real*8 function lambda_function(x)
        real*8:: x
        lambda_function = 2* x*exp(-x)*bessi1(x)*iv0(x)/(log(2*(1-exp(-x/2))*iv0(x)))
        return
    end
    
    ! evaluates the scale parameter mu
     real*8 function mu_function(x)
        real*8:: x
        mu_function = (log(2*(1-exp(-x/2))*iv0(x)))**(1/lambda_function(x))
		return
     end
     
    ! The approximation for the incomplete Weber integral multiplied by factor exp(-2*X*sinchi2)
    real*8 function q_n(X, chi0)
		implicit none
		real*8, intent(in):: X, chi0
		real*8 :: lambda, mu, arg, rad, coshi2, sinchi2, tanchi, factor, xx, y
		real*8:: expr
		parameter (rad=57.2957795130823208768d0)
		coshi2 = cos(chi0/(2.d0*rad))**2
		sinchi2 = sin(chi0/(2.d0*rad))**2
		tanchi = tan(chi0/(2.d0*rad))
		xx = X*sinchi2
		y = X*sin(chi0/rad)
		arg = y**2/(4.0*xx)
		factor = log(2*(1-exp(-arg/2.0))/(1-exp(-arg)*bessi0(arg)))

		q_n = (1-exp(-arg))*exp(-(2*mu_function(2.*arg)*xx/y)**lambda_function(2*arg))
		return 
    end function q_n
     
     
    
    !  ====================================================================
    !
    ! This function calculates the uniform asymptotic expansion for the 
    ! incomple Macdonald function, cf. [Jo07].
    !
    !  ==================================================================== 
    
      real*8 function incomplete_mcd(n, z, w)
		  implicit none
		  integer, intent(in):: n
		  real*8, intent(in) :: z, w
		  real*8:: pi, rad, alpha_0, alpha_1, beta_0, beta_1, alpha_2
		  real*8:: argument, argument1
      
      
		  parameter (pi = 3.1415926535897932384d0)
		  parameter (rad=57.2957795130823208768d0)
      
		  alpha_0 = 1.d0
		  alpha_1 = n**2/2.d0-1.d0/8.d0
		  alpha_2 = (4*n**2-1)*(4*n**2-9)/128
		  beta_0 = cosh(n*w)/sinh(w)-1/(2*sinh(w/2.d0))
      
		  beta_1 = n*sinh(n*w)/(sinh(w)**2)-cosh(n*w)*cosh(w)/(sinh(w)**3) &
			  +1/(8.*sinh(w/2.d0)**3)-alpha_1/(2*sinh(w/2.))

		  argument = sqrt(2*z)*sinh(w/2.d0)
		  argument1 = z*cosh(w)
      
		  incomplete_mcd = sqrt(pi/(2*z))*exp(-z)*(alpha_0+alpha_1/z &
			  +alpha_2/z**2)*erfc(argument)

		  incomplete_mcd = incomplete_mcd +(beta_0/z+beta_1/(z**2)) &
			  *exp(-argument1)
		  return
      end function  incomplete_mcd
      
    !  ====================================================================
    !
    ! This function is a rational approximation of the complimentary error
    ! function
    !
    !  ==================================================================== 
      
      real*8 function custom_erfc(x)
		real*8, intent(in) :: x
        
        if (x.le.8) then
            custom_erfc =(1.0606963d0+5.56438314d-1*x)/(1.0619896d0 + 1.7245609d0*x+x**2)*exp(-x**2)
        else
            custom_erfc =5.6498823d-1/(6.651874d-2+x)*exp(-x**2)
        endif
        return
      end function custom_erfc
    
end module utilities