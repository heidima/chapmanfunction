!  main.f90 
!
!****************************************************************************
!
!  PROGRAM: main
!
!  PURPOSE:  Entry point for the console application.
!  Analytic approximations are called via the approximation module
!  Exact result based on numerical calculations is called via the chapman module
!
!****************************************************************************

    program main
    USE chapman
    USE approximation
    USE chaplit
    implicit none

    ! Variables
    real*8 :: X, chi0, chi0max, A, B, HG
    integer:: i, max_n, unit
    
    chi0max = 90
    X = 1000.0   ! scale parameter  X=(R+z)/H
    max_n = 100
    unit = 1
    

    !print *, 'Exact Chapman function'
    !print *, 'Zenith angle',  'Chapman function'
    !do i=1, max_n
    !    chi0 = ((i)*chi0max)/max_n
    !    print*, chi0 , atm8_chap_num(X,chi0)
    !enddo
    
    
    print *, 'Analytic approximation, Vasylyev'
    print *, 'Zenith angle',  'Approximation of the Chapman function'
    do i=1, max_n
        chi0 = ((i)*chi0max)/max_n
        print *, chi0 , chapman_uniform_asymptotic(X,chi0)
    enddo
    
    
    !do i=1, max_n
    !    chi0 = ((i)*chi0max)/max_n
    !    print *, chi0 , chapman_k2k0_expression(X,chi0)
    !enddo
   
    !print *, 'Analytic approximation, Green'
    !print *, 'Zenith angle',  'Approximation of the Chapman function'
    !do i=1, max_n
    !    chi0 = ((i)*chi0max)/max_n
    !    print *, chi0 , green(X,chi0)
    !enddo
    !
    read*
    end program main
