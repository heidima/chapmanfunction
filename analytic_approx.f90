module approximation
    use utilities
    contains
    
!C*********************************************************************C
!C*                                                                   *C
!C*  analytic_approx.for                                              *C
!C*                                                                   *C
!C*  Written by:  Dmytro Vasylyev, Deutsches Zentrum f�r L�ft und     *C
!C*                                             Raumfahrt             *C
!C*                                                                   *C
!C*  Copyright (c) 2021  DLR                                          *C
!C*  All Rights Reserved                                              *C
!C*                                                                   *C
!C*  This software is provided on an as is basis; without any         *C
!C*  warranty; without the implied warranty of merchantability or     *C
!C*  fitness for a particular purpose.                                *C
!C*                                                                   *C
!C*********************************************************************C
!C* References can be found in chapman.f90
    

    !  ====================================================================
    !   
    !   Chapman function approximation for scale parameter 0<=X<=1.
    !   The function is discussed in the appendix and is related to
    !   approximation of the incomplete Weber integral, cf [Va13].
    !
    !  ====================================================================
    
    real*8 function chapman_small_x(X, chi0)
        implicit none
        real*8, intent(in):: X, chi0
        real*8::d, rad, B, chirad, arg, term1, term2, term3, beta, sinchi, coschi
        real*8:: ie0, ke0 , first_term
	    parameter (rad=57.2957795130823208768d0)
   
        sinchi = sin(chi0/rad)
        coschi = cos(chi0/rad)
        arg =X*sinchi
        beta = log((1.d0+coschi)/sinchi)

        ! calculate the incomplete Bessel functions Ie0 and Ke0, cf. [Ag71]
        ! q_n is the rescaled incomplete Weber integral
        ie0 = 2*q_n(X, chi0)-1.0d0+exp(-X)*bessi0(arg)

        ke0 = ie0*bessk0(arg)/bessi0(arg)+ beta -(bessk0(arg)- incomplete_mcd(0, arg, beta))/bessi0(arg)
     

        first_term = arg*exp(x)*(bessk1(arg)+beta*bessi1(arg))
        if (X.le.1.) then
            if (chi0.ne.90.d0) then
		        chapman_small_x= first_term - arg*exp(X)*(bessk1(arg)*(ie0)+bessi1(arg)*(ke0))
            else if (chi0 .eq. 90.0d0) then
                chapman_small_x = atm8_chap_xK1(X)
            end if
        else
            print*, 'Please specify X to be in range [0, 1]'
        endif    
        return
    end function chapman_small_x
    
    
    !  ====================================================================
    !   
    !   Chapman function approximation for scale parameter X>>1.
    !   The function is based on the uniform asymptotic representation
    !   of the incomplete Macdonald function, cf. [Jo07].
    !
    !  ====================================================================
    
    ! less accurate approximation based on the relation to the incomplete
    ! Macdonald functions K_0(x,y), K_2(x,y).
    ! Listed for the completeness
    
    real*8 function chapman_k2k0_expression( X, chi0 )
      
	    implicit none
        real*8, intent(in):: X, chi0
        real*8::pi, rad, sinchi, coschi, beta, sinhbeta, arg1, XC, t0sqr
	    parameter (pi = 3.1415926535897932384d0)
	    parameter (rad=57.2957795130823208768d0)

	    sinchi = sin(chi0/rad)
        coschi = cos(chi0/rad)
        beta = log((1.d0+coschi)/sinchi) 
        sinhbeta = sinh(beta)
        arg1 = X*sinchi
	    t0sqr = 2*sin( (90-chi0)/(2*rad) )**2
	    XC = sqrt(X*t0sqr)
        if (chi0.lt.90.d0) then
            chapman_k2k0_expression = -sinhbeta+ &
                arg1*exp(X)*(incomplete_mcd(2, arg1, beta) &
                -incomplete_mcd(0, arg1, beta) )/2.
            chapman_k2k0_expression = chapman_k2k0_expression * arg1  
        else if (chi0.eq.90.d0) then
            chapman_k2k0_expression =  -X*cos(chi0/rad) +&
                sqrt(1+X*(1+sinchi)) &
                *( XC + (sqrt(pi)/2)*atm8_chap_xerfc(XC) )
        endif
	    return
    end function chapman_k2k0_expression
    
    ! accurate analytic approximation based on relation to the incomplete
    ! Macdonald function K_1(x,y).
    ! Yields the same result as chapman_uniform_asymptotic(X, chi0)
    ! Listed for the completeness
    
    real*8 function chapman_k1_expression(X,chi0)
        implicit none
        real*8, intent(in):: X, chi0
        real*8::pi, rad, sinchi, coschi, beta, sinhbeta, arg1, XC, t0sqr
	    parameter (pi = 3.1415926535897932384d0)
	    parameter (rad=57.2957795130823208768d0)

	    sinchi = sin(chi0/rad)
        coschi = cos(chi0/rad)
        beta = log((1.d0+coschi)/sinchi)
        arg1 = X*sinchi
        t0sqr = 2*sin( (90-chi0)/(2*rad) )**2
	    XC = sqrt(X*t0sqr)
    
        if (chi0.lt.90.d0) then
		    chapman_k1_expression = arg1*exp(x)*incomplete_mcd(1, arg1, beta) 
        else if (chi0.eq.90.d0) then
            chapman_k1_expression =atm8_chap_xK1(X)
            !-X*coschi+&
            !      sqrt(1+X*(1+sinchi)) &
      		!    *( XC + (sqrt(pi)/2)*atm8_chap_xerfc(XC) )
          endif
        return
    end function chapman_k1_expression
    
    ! Chapman function approximation based on the uniform
    ! expansion of the incomplete Macdonald function K_1(x,y)
    ! The explicit formula (35) of current article is used
    
    real*8 function chapman_uniform_asymptotic(X, chi0)
        implicit none
        real*8, intent(in):: X, chi0
        real*8:: rad, sinchi,  arg, pi,  t,  beta
        parameter (rad=57.2957795130823208768d0)
        parameter (pi = 3.1415926535897932384d0)
    
        sinchi = sin(chi0/rad)
        t = sqrt(sinchi*(1+sinchi)/2.d0)
        arg = X*sinchi
        !beta = log((1.+coschi)/sinchi)
 

    
        if (chi0.ne.90.d0) then
            if (X.gt.600d0.and.sinchi.lt.0.3) then
                 chapman_uniform_asymptotic=1/sqrt(1-sinchi**2) ! explicit asymptotics 
            else
                chapman_uniform_asymptotic=sqrt(pi*arg/2.)*exp(X-arg)*erfc(sqrt(X-arg))*(1+3./(8*arg))
                chapman_uniform_asymptotic=chapman_uniform_asymptotic+(1-t-1/(arg*(1-sinchi**2))*(sinchi**3-t**3+3*t*(1-sinchi**2)/8))/sqrt(1-sinchi**2)
            endif
        else if (chi0 .eq. 90.0d0) then
            chapman_uniform_asymptotic  =  atm8_chap_xK1(X)
        end if
        return
    end function chapman_uniform_asymptotic
    
    
end module approximation