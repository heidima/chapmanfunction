      MODULE chaplit
      USE chapman
      USE approximation
      USE  utilities
    CONTAINS
!
!C*********************************************************************C
!C*                                                                   *C
!C*  chapman_literature.for                                           *C
!C*                                                                   *C
!C*  Written by:  David L. Huestis, Molecular Physics Laboratory      *C
!C*                                                                   *C
!C*  Copyright (c) 2000  SRI International                            *C
!C*  All Rights Reserved                                              *C
!C*                                                                   *C
!C*  This software is provided on an as is basis; without any         *C
!C*  warranty; without the implied warranty of merchantability or     *C
!C*  fitness for a particular purpose.                                *C
!C*                                                                   *C
!C*********************************************************************C
!C*
!C*	Routines to evaluate formulas from the published literature 
!C*	for the Chapman Function for the effective column depth vs 
!C*	solar zenith angle of an exponential atmosphere.
!C*
!C*  EDIT HISTORY:
!C*
!C*	01/28/2000 DLH	"Best Case" Ch31b evaluation
!C*     
!C* 02/01/2021 D. Vasylyev: added functions:
!C*          * huestis(X, chi0)
!C*          * titheridge(X, chi0)
!C*          * kocifaj(X, chi0)
!C*
!C*
!C*	See chapman.for for references.
!C*
!C*********************************************************************C


!  ====================================================================
!
!	Empirical Equation (3) of Fitzmaurice [Fi64]
!
!  ====================================================================

	real*8 function fitzmaurice( X, theta )
        implicit none
        real*8, intent(in) :: X, theta
        real*8:: pi, rad
	    !implicit real*8(a-h,o-z)
	    parameter (pi = 3.1415926535897932384d0)
	    parameter (rad=57.2957795130823208768d0)

	    fitzmaurice = sqrt(X*pi/2) &
     		    * atm8_chap_xerfc( sqrt(X/2)*cos(theta/rad) )

	return
	end


!  ====================================================================
!
!	Empirical Equation (52) of Swider [Sw64], Corrected [SG69]
!
!  ====================================================================

	real*8 function swider( X, chi0 )
      
	    implicit none
        real*8, intent(in):: X, chi0
        real*8:: pi, rad, sinchi, XC, t0sqr
	    parameter (pi = 3.1415926535897932384d0)
	    parameter (rad=57.2957795130823208768d0)

	    sinchi = sin(chi0/rad)
	    t0sqr = 2*sin( (90-chi0)/(2*rad) )**2
	    XC = sqrt(X*t0sqr)
	    swider= -X*cos(chi0/rad) + sqrt(1+X*(1+sinchi)) &
      		    *( XC + (sqrt(pi)/2)*atm8_chap_xerfc(XC) )
	    return
    end
    
!  ====================================================================
!
!	First term in asymptotic expansion of Huestis series, Section 3 of Huestis [Hu01]
!
!  ====================================================================
    
    real*8 function huestis(X, chi0)
    
        implicit none
      
        real*8, intent(in):: X, chi0
        real*8 :: pi, rad, arg, sinchi, XC, t0sqr
    
        parameter (pi = 3.1415926535897932384d0)
	    parameter (rad=57.2957795130823208768d0)
        sinchi = sin(chi0/rad)
        arg = sqrt(X*(1-sinchi))
        t0sqr = 1+sinchi
    
	    XC = sqrt(X*t0sqr)
        if (chi0.lt.90.d0) then
	        huestis = sqrt(X*pi/t0sqr)*atm8_chap_xerfc(arg)
        else if (chi0.eq.90.d0) then
              huestis =  -X*cos(chi0/rad) +&
                  sqrt(1+X*(1+sinchi)) &
      		    *( XC + (sqrt(pi)/2)*atm8_chap_xerfc(XC) )
        endif
        return
    end
    

!  ====================================================================
!
!	Empirical Equation (5) of Titheridge [Ti88], Corrected [Ti00]
!
!  ====================================================================
    
    real*8 function titheridge(X, chi0)
        implicit none
        real*8, intent(in):: X, chi0
        real*8::d, rad, B, chirad,pi
	    parameter (rad=57.2957795130823208768d0)
        parameter (pi = 3.1415926535897932384d0)
    
        chirad = chi0/rad
        B = 1.0123d0-1.453/sqrt(X)
        d= 3.88d0*180./pi*X**(-1.143d0)*(1/cos(B*chirad )-0.834d0)
        d = d/rad
        titheridge = 1/cos(chirad -d)
    
        return 
    end
    
!  ====================================================================
!
!	Empirical Equation (11b) of Kocifaj [Ti96] 
!
!  ====================================================================
    
    real*8 function kocifaj(X, chi0)
        implicit none
        real*8, intent(in):: X, chi0
        real*8:: rad, sinchi, coschi, arg, pi
        parameter (rad=57.2957795130823208768d0)
        parameter (pi = 3.1415926535897932384d0)
        sinchi = sin(chi0/rad)
        coschi = cos(chi0/rad)
        arg = sqrt(X/2.)*coschi
        if (chi0.ne.90.d0) then
		    kocifaj = (coschi+sqrt(pi*X/2.)*(1./X+1.+sinchi**2)*exp(arg*arg)*custom_erfc(arg))/2
        else if (chi0 .eq. 90.0d0) then
            kocifaj = atm8_chap_xK1(X)
        end if

    
        return
    end function kocifaj
    
    
    !  ====================================================================
    !
    !	Empirical Equation (11) of Kocifaj [Gr69] 
    !
    !  ====================================================================
    real*8 function green(X, chi0)
    implicit none
        real*8, intent(in):: X, chi0
        real*8:: ch, alpha, rad, chi, pi
        parameter (rad=57.2957795130823208768d0)
        parameter (pi = 3.1415926535897932384d0)
        
        chi = chi0/rad
        
        alpha = (16/pi**4)*(1-(pi/2)**2*(0.115d0+1.0d0/log(pi/(2)*X)))
        
        green = exp(chi**2/(2*(1-0.115d0*chi**2-alpha*chi**4)))
        return
    end function green
        
    
      end MODULE chaplit