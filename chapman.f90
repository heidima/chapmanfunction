      
       
!C*********************************************************************C
!C*                                                                   *C
!C*  chapman.for                                                      *C
!C*                                                                   *C
!C*  Written by:  David L. Huestis, Molecular Physics Laboratory      *C
!C*                                                                   *C
!C*  Copyright (c) 2000  SRI International                            *C
!C*  All Rights Reserved                                              *C
!C*                                                                   *C
!C*  This software is provided on an as is basis; without any         *C
!C*  warranty; without the implied warranty of merchantability or     *C
!C*  fitness for a particular purpose.                                *C
!C*                                                                   *C
!C*********************************************************************C
!C*
!C*	To calculate the Chapman Function, Ch(X,chi0), the column 
!C*	depth of an exponential atmosphere integrated along a line 
!C*	from a given point to the sun, divided by the column depth for 
!C*	a vertical sun.
!C*
!C*  USAGE:
!C*
!C*	  z = altitude above the surface
!C*	  R = radius of the planet
!C*	  H = atmospheric scale height
!C*
!C*	  X = (R+z)/H
!C*	  chi0 = solar zenith angle (in degrees)
!C*
!C*	  implicit real*4(a-h,o-z)
!C*	  depth = atm_chapman(X,chi0)	! analytical
!C*	  depth = atm_chap_num(X,chi0)	! numerical (chi0 .le. 90)
!C*
!C*	  implicit real*8(a-h,o-z)
!C*	  depth = atm8_chapman(X,chi0)	! analytical
!C*	  depth = atm8_chap_num(X,chi0)	! numerical (chi0 .le. 90)
!C*
!C*
!C*    CODING
!C*
!C*	No claims are made that the code is optimized for speed, 
!C*	accuracy, or compactness.  The principal objectives were 
!C*
!C*	  (1) Robustness with respect to argument values
!C*	  (2) Rigorous mathematical derivation and error control
!C*	  (3) Maximal use of "well known" mathematical functions
!C*	  (4) Ease of readability and mapping of theory to coding
!C*
!C*	The real*8 accuracy could be improved with more accurate 
!C*	representations of E1(), erfc(), I0(), I1(), K0(), K1().
!C*
!C*	In the course of development, many representations and 
!C*	approximations of the Chapman Function were attempted that 
!C*	failed to be robustly extendable to machine-precision.
!C*
!C*  INTERNET ACCESS:
!C*
!C*	Source: http://www-mpl.sri.com/software/chapman/chapman.html
!C*	Author: mailto:david.huestis@sri.com
!C*	        http://www-mpl.sri.com/bios/Huestis-DL.html
!C*
!C*  EDIT HISTORY:
!C*
!C*	01/22/2000 DLH	First complete documentation
!C*
!C*	01/15/2000 DLH	First complete version of chapman.for
!C*
!C**********************************************************************
!C*
!C*  THEORY:
!C*
!C*    INTRODUCTION
!C*
!C*	    This computer code models the absorption of solar radiation 
!C*	by an atmosphere that depends exponentionally on altitude.  In 
!C*	specific we calculate the effective column depth of a species 
!C*	of local density, n(z), from a point at a given altitude, z0, 
!C*	to the sun at a given solar zenith angle, chi0.  Following Rees 
!C*	[Re89, Section 2.2] we write the column depth for chi0 .le. 90 
!C*	degrees as
!C*
!C*   (A)  N(z0,chi0) = int{z=z0,infinity} 
!C*	     [ n(z)/sqrt( 1 - ( sin(chi0) * (R+z0) / (R+z) ) **2 ) dz ]
!C*
!C*	where R is the radius of the solid planet (e.g. Earth).  For 
!C*	chi0 .gt. 90 degrees we write
!C*
!C*	  N(z0,chi0) = 2*N(zs,90) - N(z0,180-chi0)
!C*
!C*	where zs = (R+z0)*sin(chi0)-R is the tangent height.
!C*
!C*	    For an exponential atmosphere, with
!C*
!C*	  n(z) = n(z0) * exp(-(z-z0)/H)
!C*
!C*	with a constant scale height, H, the column depth can be 
!C*	represented by the Chapman function, Ch(X,chi0), named after 
!C*	the author of the first quantitative mathematical investigation 
!C*	[Ch31b] trough the relation
!C*
!C*	  N(z0,chi0) = H * n(z0) * Ch(X,chi0)
!C*
!C*	where X = (R+z0)/H is a dimensionless measure of the radius 
!C*	of curvature, with values from about 300 to 1300 on Earth.
!C*
!C*
!C*    APPROACH
!C*
!C*	    We provide function entry points for very stable and 
!C*	reasonably efficient evaluation of Ch(X,chi0) with full 
!C*	single-precision accuracy (.le. 6.0E-7 relative) for a wide 
!C*	range of parameters.  A 15-digit-accurate double precision 
!C*	numerical integration routine is also provided.
!C*
!C*	    Below we will develop (1) a compact asymptotic expansion of 
!C*	good accuracy for moderately large values of X (.gt. 36) and all 
!C*	values of chi0, (2) an efficient numerical integral for 
!C*	all values of X and chi0, and (3) an explicit analytical 
!C*	representation, valid for all values of X and chi0, based 
!C*	the differential equation satisfied by Ch(X,chi0).
!C*
!C*	    All three of these represent new research results as well 
!C*	as significant computational improvements over the previous 
!C*	literature, much of which is cited below.
!C*
!C*
!C*    CHANGES OF THE VARIABLE OF INTEGRATION
!C*
!C*	Substituting y = (R+z)/(R+z0) - 1 we find
!C*
!C*   (B)  Ch(X,chi0) = X * int{y=0,infinity}
!C*	     [ exp(-X*y) / sqrt( 1 - ( sin(chi0) / (1+y) )**2 ) dy ]
!C*
!C*	The futher substitutions s = (1+y)/sin(chi0), s0 = 1/sin(chi0) 
!C*	give
!C*
!C*   (C)  Ch(X,chi0) = X*sin(chi0) * int{s=s0,infinity}
!C*	     [ exp(X*(1-sin(chi0)*s)) * s / sqrt(s**2-1) ds ]
!C*
!C*	From this equation we can establish that
!C*
!C*	  Ch(X,90) = X*exp(X)*K1(X)
!C*
!C*	[AS64, Equations 9.6.23 and 9.6.27].  If we now substitute
!C*	s = 1/sin(lambda) we obtain
!C*
!C*   (D)  Ch(X,chi0) = X*sin(chi0) * int{lambda=0,chi0} 
!C*	    [ exp(X*(1-sin(chi0)*csc(lambda))) * csc(lambda)**2 dlambda]
!C*
!C*	which is the same as Chapman's original formulation [Ch31b, p486,
!C*	eqn (10)].  If we first expand the square root in (B)
!C*
!C*	  1/sqrt(1-q) = 1 + q/( sqrt(1-q)*(1+sqrt(1-q)) )
!C*
!C*	with q = ( sin(chi0) / (1+y) )**2 = sin(lambda)**2, we obtain 
!C*	a new form of (D) without numerical sigularities and simple 
!C*	convergence to Ch(0,chi0) = Ch(X,0) = 1
!C*
!C*   (E)  Ch(X,chi0) = 1 + X*sin(chi0) * int{lambda=0,chi0} 
!C*	    [ exp(X*(1-sin(chi0)*csc(lambda))) 
!C*		/ (1 + cos(lambda) ) dlambda ]
!C*
!C*	Alternatively, we may substitute t**2 = y + t0**2, 
!C*	into Equation (B), with t0**2 = 1-sin(chi0), finding
!C*
!C*   (F)  Ch(X,chi0) = X * int{s=t0,infinity} 
!C*	    [ exp(-X*(t**2-t0**2)) * f(t,chi0) dt ]
!C* 
!C*	where
!C*
!C*	  f(t,chi0) = (t**2 + sin(chi0)) / sqrt(t**2+2*sin(chi0))
!C*
!C*	  f(t,chi0) = (t**2-t0**2+1)/sqrt(t**2-t0**2+1+sin(chi0))
!C*
!C*	    Below we will use Equation (F) above to develop a
!C*	compact asymptotic expansion of good accuracy for moderately 
!C*	large values of X (.gt. 36) and all values of chi0, Equation (E) 
!C*	to develop an efficient numerical integral for Ch(X,chi0) for 
!C*	all values of X and chi0, and Equation (C) to derive an explicit 
!C*	analytical representation, valid for all values of X and chi0,  
!C*	based on the differential equation satisfied by Ch(X,chi0).
!C*
!C*    atm_chapman(X,chi0) and atm8_chapman(X,chi0)
!C*
!C*	These routines return real*4 and real*8 values of Ch(X,chi0)
!C*	selecting the asymptotic expansion or differential equation 
!C*	approaches, depending on the value of X.  These routines also 
!C*	handle the case of chi0 .gt. 90 degrees.
!C*
!C*    atm_chap_num(X,chi0) and atm8_chap_num(X,chi0)
!C*
!C*	These routines return real*4 and real*8 values of Ch(X,chi0)
!C*	evaluated numerically.  They are both more accurate than the 
!C*	corresponding atm*_chapman() functions, but take significantly 
!C*	more CPU time.
!C*
!C*
!C*    ASYMPTOTIC EXPANSION
!C*
!C*	From Equation (F) we expand, with t0**2 = 1-sin(chi0), 
!C*
!C*	  f(t,chi0) = sum{n=0,3} [ C(n,chi0) * (t**2-t0**2)**n ]
!C*
!C*	The function atm8_chap_asy(X,chi0) evaluates integrals of the 
!C*	form
!C*
!C*	  int{t=t0,infinity} [exp(-X*(t**2-t0**2))*(t**2-t0**2)**n dt]
!C*
!C*	in terms of incomplete gamma functions, and sums them to 
!C*	compute Ch(X,chi0).  For large values of X, this results in an 
!C*	asymptotic expansion in negative powers of X, with coefficients 
!C*	that are stable for all values of chi0.
!C*
!C*	In contrast, the asymptotic expansions of Chapman [Ch31b, 
!C*	p488, Equation (22) and p490, Equation (38)], Hulburt [He39], 
!C*	and Swider [Sw64, p777, Equation (43)] use negative powers of 
!C*	X*cos(chi0)**2 or X*sin(chi0), and are accurate only for 
!C*	small values or large values of chi0, respectively.
!C*
!C*	Taking only the first term in the present expansion gives the 
!C*	simple formula
!C*
!C*	  Ch(X,chi0) = sqrt(pi*X/(1+sin(chi0))) * exp(X*(1-sin(chi0)))
!C*		* erfc( sqrt(X*(1-sin(chi0))) )
!C*
!C*	This is slightly more accurate than the semiempirical 
!C*	formula of Fitzmaurice [Fi64, Equation (3)], and sightly less 
!C*	accurate than that of Swider [Sw64, p780, Equation (52), 
!C*	corrected in SG69].
!C*
!C*
!C*    NUMERICAL INTEGRATION
!C*
!C*	We are integrating
!C*
!C*   (E)  Ch(X,chi0) = 1 + X*sin(chi0) * int{lambda=0,chi0} 
!C*	    [ exp(X*(1-sin(chi0)*csc(lambda))) 
!C*		/ ( 1 + cos(lambda) ) dlambda ]
!C*
!C*	The integrand is numerically very smooth, and rapidly varying 
!C*	only near lambda = 0.  For X .ne. 0 we choose the lower limit 
!C*	of numerical integration such that the integrand is 
!C*	exponentially small, 7.0E-13 (3.0E-20 for real*8).  The domain 
!C*	of integration is divided into 64 equal intervals (6000 for 
!C*	real*8), and integrated numerically using the 9-point closed 
!C*	Newton-Cotes formula from Hildebrand [Hi56a, page 75, Equation
!C*	(3.5.17)].
!C*
!C*
!C*    INHOMOGENOUS DIFFERENTIAL EQUATION
!C*
!C*	    The function atm8_chap_deq(X,chi0) calculates Ch(X,chi0), 
!C*	based on Equation (C) above, using the inhomogeneous 
!C*	Bessel's equation as described below.  Consider the function 
!C*
!C*	  Z(Q) = int{s=s0,infinity} [ exp(-Q*s) / sqrt(s**2-1) ds ]
!C*
!C*	Differentiating with respect to Q we find that 
!C*
!C*	  Ch(X,chi0) = - Q * exp(X) * d/dQ [ Z(Q) ]
!C*
!C*	with Q = X*sin(chi0), s0 = 1/sin(chi0).  Differentiating 
!C*	inside the integral, we find that
!C*
!C*	  Z"(Q) + Z'(Q)/Q - Z(Q) = sqrt(s0**2-1) * exp(-Q*s0) / Q
!C*
!C*	giving us an inhomogeneous modified Bessel's equation of order 
!C*	zero.  Following Rabenstein [Ra66, pp43-45,149] the solution 
!C*	of this equation can be written as
!C*
!C*	  Z(Q) = A*I0(Q) + B*K0(Q) - sqrt(s0**2-1) 
!C*	         * int{t=Q,infinity} [ exp(-t*s0) 
!C*		   * ( I0(Q)*K0(t) - I0(t)*K0(Q) ) dt ] 
!C*
!C*	with coefficients A and B to be determined by matching 
!C*	boundary conditions.
!C*
!C*	    Differentiating with respect to Q we obtain
!C*
!C*	  Ch(X,chi0) = X*sin(chi0)*exp(X)*( 
!C*		- A*I1(X*sin(chi0)) + B*K1(X*sin(chi0)) 
!C*		+ cos(chi0) * int{y=X,infinity} [ exp(-y) 
!C*		  * ( I1(X*sin(chi0))*K0(y*sin(chi0))
!C*		    + K1(X*sin(chi0))*I0(y*sin(chi0)) ) dy ] )
!C*
!C*	Applying the boundary condition Ch(X,0) = 1 requires that 
!C*	B = 0.  Similarly, the requirement that Ch(X,chi0) approach 
!C*	the finite value of sec(chi0) as X approaches infinity [Ch31b, 
!C*	p486, Equation (12)] implies A = 0.  Thus we have
!C*
!C*	  Ch(X,chi0) = X*sin(chi0)*cos(chi0)*exp(X)*
!C*		int{y=X,infinity} [ exp(-y) 
!C*		  * ( I1(X*sin(chi0))*K0(y*sin(chi0))
!C*		    + K1(X*sin(chi0))*I0(y*sin(chi0)) ) dy ]
!C*
!C*	The function atm8_chap_deq(X,chi0) evaluates this expression.
!C*	Since explicit approximations are available for I1(z) and K1(z),
!C*	the remaining challenge is evaluation of the integrals
!C*
!C*	  int{y=X,infinity} [ exp(-y) I0(y*sin(chi0)) dy ]
!C*
!C*	and
!C*
!C*	  int{y=X,infinity} [ exp(-y) K0(y*sin(chi0)) dy ]
!C*
!C*	which are accomplished by term-by-term integration of ascending
!C*	and descending power series expansions of I0(z) and K0(z).
!C*
!C*  REFERENCES:
!C*
!C*	AS64	M. Abramowitz and I. A. Stegun, "Handbook of 
!C*		Mathematical Functions," NBS AMS 55 (USGPO, 
!C*		Washington, DC, June 1964, 9th printing, November 1970).
!C*
!C*	Ch31b	S. Chapman, "The Absorption and Dissociative or
!C*		Ionizing Effect of Monochromatic Radiation in an
!C*		Atmosphere on a Rotating Earth: Part II. Grazing
!C*		Incidence," Proc. Phys. Soc. (London), _43_, 483-501 
!C*		(1931).
!C*
!C*	Fi64	J. A. Fitzmaurice, "Simplfication of the Chapman
!C*		Function for Atmospheric Attenuation," Appl. Opt. _3_,
!C*		640 (1964).
!C*
!C*	Hi56a	F. B. Hildebrand, "Introduction to Numerical
!C*		Analysis," (McGraw-Hill, New York, 1956).
!C*
!C*	Hu39	E. O. Hulburt, "The E Region of the Ionosphere," 
!C*		Phys. Rev. _55_, 639-645 (1939).
!C*
!C*	PFT86	W. H. Press, B. P. Flannery, S. A. Teukolsky, and 
!C*		W. T. Vetterling, "Numerical Recipes," (Cambridge, 
!C*		1986).
!C*
!C*	Ra66	A. L. Rabenstein, "Introduction to Ordinary
!C*		Differential Equations," (Academic, NY, 1966).
!C*
!C*	Re89	M. H. Rees, "Physics and Chemistry of the Upper
!C*		Atmosphere," (Cambridge, 1989).
!C*
!C*	SG69	W. Swider, Jr., and M. E. Gardner, "On the Accuracy 
!C*		of Chapman Function Approximations," Appl. Opt. _8_,
!C*		725 (1969).
!C*
!C*	Sw64	W. Swider, Jr., "The Determination of the Optical 
!C*		Depth at Large Solar Zenith Angles," Planet. Space 
!C*		Sci. _12_, 761-782 (1964).
!C*
!C* Hu01 D. L. Huestis, "Accurate evaluation of the Chapman
!C*     function for atmospheric attenuation", J. Quant. Spectr.
!C*     Radiat. Transfer _69_, 709-721 (2001).
!C*
!C* Ti88 J. E. Titheridge, "An approximate form for the Chapman
!C*		grazing incidence function", J. Atm. Terr. Phys. _50_,
!C*     699-701 (1988).
!C*
!C* Ti00 J. E. Titheridge, "Modeling the peak of the ionospheric
!C*     E-layer", J. Atm. Terr. Phys. _62_, 93-114 (2000).
!C*
!C* Ko96 M. Kocifaj, "Optical air mass and refraction in a Rayleigh
!C*		atmosphere", Contrib. Astron. Obs. Skalnate Pleso _26_, 23-30 (1996). 
!C* 
!C* Gr69 A.E.S. Green, C.S. Lindenmeyer, and M. Griggs, "Molecular Absorption in 
!C*		Planetary Atmospheres", J. Geophys. Res. _69_, 493 (1969).
!C*
!C* Va13 D. Vasylyev, A. Semenov, and W. Vogel, "Quantum channels with beam wandering:
!C*		an analysis of the Marcum Q-function", Phys. Scripta _T153_, 014062 (2013).
!C*
!C* Ag71 M. Agrest and M. Maximov, "Theory of Incomplete Cylindrical Functions
!C*     and Their Applications, Springer, Berlin, 1971.
!C* 
!C* Jo07 D. Jones, "Incomplete Bessel functions, I", Proc. Edinburgh Mat. Soc. _50_,
!C*		173-183 (2007).
    
!C*********************************************************************C
!C*                                                                   *C
!C*  chapman.f90                                                      *C
!C*                                                                   *C
!C*  Written by:  David L. Huestis, Molecular Physics Laboratory      *C
!C*                                                                   *C
!C*  Copyright (c) 2000  SRI International                            *C
!C*  All Rights Reserved                                              *C
!C*                                                                   *C
!C*  This software is provided on an as is basis; without any         *C
!C*  warranty; without the implied warranty of merchantability or     *C
!C*  fitness for a particular purpose.                                *C
!C*                                                                   *C
!C*********************************************************************C
!   The module calculates the Chapman function
!
!	  Ch(X,chi0) = atm8_chap_num(X,chi0) : real*8 numerical integral
!
!  ####################################################################
!
!  ====================================================================
!
!	This is the function for the user to call.
!
!	chi0 can range from 0 to 180 in degrees.  For chi0 .gt. 90, the 
!	product X*(1-sin(chi0)) must not be too large, otherwise we 
!	will get an exponential overflow.
!
!	For chi0 .le. 90 degrees, X can range from 0 to thousands 
!	without overflow.
!
!  ====================================================================
      
    MODULE chapman
    
    ! This module calculates the Chapman function using numerical integration
    ! The code is written originally by David L. Huestis 
    ! Some parts of code have been modified for clearer definition of interfaces

    CONTAINS

!  ====================================================================
!
!	This Chapman function routine calculates
!
!	  Ch(X,chi0) = atm8_chap_num(X,chi0) = numerical integral 
!
!    NUMERICAL INTEGRATION
!
!	We are integrating
!
!       Ch(X,chi0) = 1 + X*sin(chi0) * int{lambda=0,chi0} 
!	    [ exp(X*(1-sin(chi0)*csc(lambda))) 
!		/ ( 1 + cos(lambda) ) dlambda ]
!
!	The integrand is numerically very smooth, and rapidly varying 
!	only near lambda = 0.  For X .ne. 0 we choose the lower limit 
!	of numerical integration such that the integrand is 
!	exponentially small, 7.0E-13 (3.0E-20 for real*8).  The domain 
!	of integration is divided into 64 equal intervals (6000 for 
!	real*8), and integrated numerically using the 9-point closed 
!	Newton-Cotes formula from Hildebrand [Hi56a, page 75, Equation
!	(3.5.17)].   
!   Hi56a	F. B. Hildebrand, "Introduction to Numerical
!		Analysis," (McGraw-Hill, New York, 1956).
!
!  ====================================================================

	real*8 function atm8_chap_num(X,chi0)
	implicit real*8(a-h,o-z)
    real*8, intent(in) :: X, chi0
	parameter (rad=57.2957795130823208768D0)
	parameter (n=601,nfact=8)
	dimension factor(0:nfact)
	data factor/14175.0D0, 23552.0D0, -3712.0D0, 41984.0D0, &
      	  -18160.0D0, 41984.0D0, -3712.0D0, 23552.0D0, 7912.0D0/

	if( (chi0 .le. 0) .or. (chi0 .gt. 90) .or. (X .le. 0) ) then
	  atm8_chap_num = 1
	  return
	end if

	chi0rad = chi0/rad
	sinchi = sin(chi0rad)

	alpha0 = asin( (X/(X+45)) * sinchi )
	delta = (chi0rad - alpha0)/(n-1)

	sum = 0

	do i=1,n
	  alpha = -(i-1)*delta + chi0rad

	  if( (i .eq. 1) .or. (X .le. 0) ) then
	    f = 1/(1+cos(alpha))
	  else if( alpha .le. 0 ) then
	    f = 0
	  else
	    f = exp(-X*(sinchi/sin(alpha)-1) ) /(1+cos(alpha))
	  end if

	  if( (i.eq.1) .or. (i.eq.n) ) then
	    fact = factor(nfact)/2
	  else
	    fact = factor( mod(i-2,nfact)+1 )
	  end if

	  sum = sum + fact*f
	end do

	atm8_chap_num = 1 + X*sinchi*sum*delta/factor(0)
	return
	end

    END MODULE chapman